package com.training.webservice.services;

import com.training.webservice.model.ClientModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ClientServices {

    public ResponseEntity<ClientModel> getData(String documentType, Integer document){
        ClientModel cliente = new ClientModel();
        cliente.setFirstName("Juan");
        cliente.setSecondName("Perez");
        return ResponseEntity.ok(cliente);
    }

    public ResponseEntity<ClientModel> getDataCliente(String documentType, String document){
        try {
            if(documentType.equals("C") || documentType.equals("P")){
                if(document.equals("12345678")){
                    ClientModel cliente = new ClientModel();
                    cliente.setFirstName("Juan");
                    cliente.setSecondName("Jose");
                    cliente.setPrimerApellido("Perez");
                    cliente.setSegundoApellido("Yepez");
                    cliente.setNumberPhone("+1 866-994-3157");
                    cliente.setDireccion("6515 International Dr, Orlando, FL 32819, Estados Unidos");
                    cliente.setCiudad("Orlando, Estados Unidos");
                    return new ResponseEntity<>(cliente, HttpStatus.resolve(200));
                } else {
                    return new ResponseEntity<>(HttpStatus.resolve(404));
                }
            } else {
                return new ResponseEntity<>(HttpStatus.resolve(400));
            }
        }catch (Exception e){
            System.out.println(e);
            return new ResponseEntity<>(HttpStatus.resolve(500));
        }
    }

}
